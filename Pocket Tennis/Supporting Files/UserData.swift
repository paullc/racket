//
//  UserData.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 3/10/21.
//

import Combine
import SwiftUI
import FirebaseDatabase
import Firebase
import FirebaseStorage

let database = Database.database().reference()
let storage = Storage.storage().reference()

let firstColor = UIColor(displayP3Red: 69/255, green: 162/255, blue: 71/255, alpha: 1)
let secondColor = UIColor(displayP3Red: 40/255, green: 60/255, blue: 134/255, alpha: 1)
let backgroundGradient = LinearGradient(gradient: Gradient(colors: [Color(firstColor), Color(secondColor)]), startPoint: .top, endPoint: .bottom)


final class UserData: ObservableObject {
    
    // Publish if the user is authenticated or not
    @Published var userAuthenticated = false
    @Published var currentUser = DbUser(isAdmin: false)
    @Published var allUsers = [String: DbUser]()
    @Published var allReviews = [String: [String: LessonReview]]()
    @Published var currentUserReviews = [String: LessonReview]()
    
}
