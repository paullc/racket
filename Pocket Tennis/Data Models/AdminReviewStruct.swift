//
//  AdminReviewStruct.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/18/21.
//

import SwiftUI


struct AdminReview: Codable {
    
    // Types of hits
    var forehandCurr: Bool
    var forehandNext: Bool
    
    var backhandCurr: Bool
    var backhandNext: Bool
    
    var forehandVolleyCurr: Bool
    var forehandVolleyNext: Bool
    
    var backhandVolleyCurr: Bool
    var backhandVolleyNext: Bool
    
    var overhandCurr: Bool
    var overhandNext: Bool

    var reverseForehandCurr: Bool
    var reverseForehandNext: Bool

    var dropshotCurr: Bool
    var dropshotNext: Bool

    var lobCurr: Bool
    var lobNext: Bool

    var serveCurr: Bool
    var serveNext: Bool

    var returnServeCurr: Bool
    var returnServeNext: Bool

    
    // Skills
    var quicknessCurr: Bool
    var quicknessNext: Bool
    
    var agilityCurr: Bool
    var agilityNext: Bool

    var balanceCurr: Bool
    var balanceNext: Bool

    var reactionTimeCurr: Bool
    var reactionTimeNext: Bool

    var aerobicCurr: Bool
    var aerobicNext: Bool

    var strategyCurr: Bool
    var strategyNext: Bool

    var mentalToughnessCurr: Bool
    var mentalToughnessNext: Bool

    
    //Notes
    var notes: String
    
    init() {
        forehandCurr  = false
        forehandNext  = false
        
        backhandCurr = false
        backhandNext = false
        
        forehandVolleyCurr = false
        forehandVolleyNext = false
        
        backhandVolleyCurr = false
        backhandVolleyNext = false
        
        overhandCurr = false
        overhandNext = false

        reverseForehandCurr = false
        reverseForehandNext = false

        dropshotCurr = false
        dropshotNext = false

        lobCurr = false
        lobNext = false

        serveCurr = false
        serveNext = false

        returnServeCurr = false
        returnServeNext = false

        
        // Skills
        quicknessCurr = false
        quicknessNext = false
        
        agilityCurr = false
        agilityNext = false

        balanceCurr = false
        balanceNext = false

        reactionTimeCurr = false
        reactionTimeNext = false

        aerobicCurr = false
        aerobicNext = false

        strategyCurr = false
        strategyNext = false

        mentalToughnessCurr = false
        mentalToughnessNext = false
        
        notes = ""
    }
    
    func toDictionary() -> [String: Any] {
        
        // Types of hits
        return [
         "forehandCurr" : forehandCurr,
         "forehandNext" : forehandNext,
        
         "backhandCurr" : backhandCurr,
         "backhandNext" : backhandNext,
        
         "forehandVolleyCurr" : forehandVolleyCurr,
         "forehandVolleyNext" : forehandVolleyNext,
        
         "backhandVolleyCurr" : backhandVolleyCurr,
         "backhandVolleyNext" : backhandVolleyNext,
        
         "overhandCurr" : overhandCurr,
         "overhandNext" : overhandNext,

         "reverseForehandCurr" : reverseForehandCurr,
         "reverseForehandNext" : reverseForehandNext,

         "dropshotCurr" : dropshotCurr,
         "dropshotNext" : dropshotNext,

         "lobCurr" : lobCurr,
         "lobNext" : lobNext,

         "serveCurr" : serveCurr,
         "serveNext" : serveNext,

         "returnServeCurr" : returnServeCurr,
         "returnServeNext" : returnServeNext,

        
        // Skills
         "quicknessCurr" : quicknessCurr,
         "quicknessNext" : quicknessNext,
        
         "agilityCurr" : agilityCurr,
         "agilityNext" : agilityNext,

         "balanceCurr" : balanceCurr,
         "balanceNext" : balanceNext,

         "reactionTimeCurr" : reactionTimeCurr,
         "reactionTimeNext" : reactionTimeNext,

         "aerobicCurr" : aerobicCurr,
         "aerobicNext" : aerobicNext,

         "strategyCurr" : strategyCurr,
         "strategyNext" : strategyNext,

         "mentalToughnessCurr" : mentalToughnessCurr,
         "mentalToughnessNext" : mentalToughnessNext,
            "notes" : notes.isEmpty ? "None" : notes
        ]
    }
    
}
