//
//  ClientReviewStruct.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/18/21.
//

import SwiftUI

struct ClientReview: Codable {
    
    var head: Double
    var heart: Double
    var lungs: Double
    var legs: Double
    var funMeter: Double

    
    //Notes
    var notes: String
    
    init() {
        head = 1.0
        heart = 1.0
        lungs = 1.0
        legs = 1.0
        funMeter = 1.0
        notes = ""
    }
    
    func toDictionary() -> [String: Any] {
        
        return [
            "head" : head,
            "heart": heart,
            "lungs" : lungs,
            "legs" : legs,
            "funMeter" : funMeter,
            "notes" : notes.isEmpty ? "None" : notes
        ]
    }
    
}
