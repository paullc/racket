//
//  ClientReviewList.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/20/21.
//

import SwiftUI

struct ClientReviewList: View {
    @EnvironmentObject var userData: UserData
    @State private var searchItem = ""


    var body: some View {
        NavigationView {
            if userData.currentUserReviews.isEmpty {
                VStack {
//                    HStack {
//                        Text("You have no reviews to display.\n Reviews will appear here after your first lesson")
//                            .multilineTextAlignment(.center)
//                            .frame(width: 200, alignment: .center)
//                            .font(Font.subheadline.weight(.semibold))
//                            .padding(10)
//                            .foregroundColor(.white)
//                            .background(
//                                RoundedRectangle(cornerRadius: 16)
//                                    .foregroundColor(.accentColor)
//                                    .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
//                            )
//                    } // HStack
                    HStack {
                        Text("You have no reviews to display. Reviews will appear here after your first lesson")
                            .multilineTextAlignment(.center)
                            .frame(width: 200, alignment: .center)
                            .font(Font.subheadline.weight(.semibold))
                            .padding(10)
                            .opacity(0.6)
                    } // HStack
                } // VStack
                .navigationBarTitle(Text("My Reviews"))

            }
            else {
                ZStack {
                    backgroundGradient
                        .edgesIgnoringSafeArea(.all)
                    
                    VStack {
                        SearchBar(placeholder: "Search reviews by date or instructor", text: $searchItem)
                        List {
                            ForEach(Array(userData.currentUserReviews.values).filter { aReview in
                                return searchItem.isEmpty
                                    || aReview.submittedBy.localizedStandardContains(searchItem)
                                    || aReview.lessonDate.localizedStandardContains(searchItem)
                            }.sorted(by: { reviewA, reviewB in
                                return reviewA.lessonDate > reviewB.lessonDate
                            }), id: \.id) { aLessonReview in
                                NavigationLink(destination: ReviewDetails(user: userData.currentUser, lessonReview: aLessonReview)) {
                                    ReviewItem(user: userData.currentUser, lessonReview: aLessonReview)
                                }
                            }
                            
                        }
                        .listStyle(InsetGroupedListStyle())
                    } // VStack
                } // ZStack
                .navigationBarTitle(Text("My Reviews"))

            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

