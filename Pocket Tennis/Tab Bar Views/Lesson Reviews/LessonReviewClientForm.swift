//
//  LessonReviewClientForm.swift
//  Racket
//
//  Created by Harlan  Reese Pounders on 3/18/21.
//

import SwiftUI
import FirebaseDatabase

struct LessonReviewClientForm: View {
    let lessonReview: LessonReview
    @State private var clientReview = ClientReview()
    @EnvironmentObject var userData: UserData
    @Environment(\.presentationMode) var presentationMode

    
    @State private var isEditing = false
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("On a scale from 1-5...")) {
                    HStack (alignment: .center, spacing: 43) {
                        VStack {
                            Text("1")
                            Text("Bad")
                        }
                        VStack {
                            Text("2")
                            Spacer()
                        }
                        VStack {
                            Text("3")
                            Text("Okay")
                        }
                        VStack {
                            Text("4")
                            Spacer()
                        }
                        VStack {
                            Text("5")
                            Text("Great")
                        }
                    } //End of HStack
                } // End of Section
                Section {
                    Text("How did your ___ feel?")
                    VStack {
                        Text("Head")
                        Slider(value: self.$clientReview.head, in: 1...5, step: 1, onEditingChanged: { editing in isEditing = editing })
                        Text("\(Int(self.clientReview.head))")
                    }
                    VStack {
                        Text("Heart")
                        Slider(value: self.$clientReview.heart, in: 1...5, step: 1, onEditingChanged: { editing in isEditing = editing })
                        Text("\(Int(self.clientReview.heart))")
                    }
                    VStack {
                        Text("Lungs")
                        Slider(value: self.$clientReview.lungs, in: 1...5, step: 1, onEditingChanged: { editing in isEditing = editing })
                        Text("\(Int(self.clientReview.lungs))")
                    }
                    VStack {
                        Text("Legs")
                        Slider(value: self.$clientReview.legs, in: 1...5, step: 1, onEditingChanged: { editing in isEditing = editing })
                        Text("\(Int(self.clientReview.legs))")
                    }
                } //End of Section
                Section(header: Text("How much fun did you have?")) {
                    HStack (alignment: .center, spacing: 40) {
                        VStack {
                            Text("1")
                            Text("None")
                        }
                        VStack {
                            Text("2")
                            Text("")
                        }
                        VStack {
                            Text("3")
                            Text("Some")
                                .minimumScaleFactor(0.3)
                                .lineLimit(1)
                        }
                        VStack {
                            Text("4")
                            Text("")
                        }
                        VStack {
                            Text("5")
                            Text("A ton")
                        }
                    } //End of HStack
                    VStack {
                        Text("Fun Meter")
                        Slider(value: self.$clientReview.funMeter, in: 1...5, step: 1, onEditingChanged: { editing in isEditing = editing })
                        Text("\(Int(self.clientReview.funMeter))")
                    }
                } // End of section
                Section(header: Text("Notes")) {
                    TextField("Notes", text: self.$clientReview.notes)
                        .font(.system(size: 20))

                }
                
                Button(action: {
                    publishClientReviewForUser()
                }) {
                    HStack {
                        Spacer()
                        Text("Submit")
                        Spacer()
                    }
                }
            } // End of Form
            .navigationBarTitle("Complete Review")
        } // End of NavigationView
    } // End of body
    
    func removeTrailingZeros(numberToTrim: Double) -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: numberToTrim)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 16 //maximum digits in Double after dot (maximum precision)
        return String(formatter.string(from: number) ?? "")
    }
    
    func publishClientReviewForUser() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMM, y"
        
        
        let clientReviewDict = self.clientReview.toDictionary() // Gets a dict representaiton of the review
        database.child("reviews").child(userData.currentUser.id).child(lessonReview.id).child("client").setValue(clientReviewDict) {
            (error: Error?, ref:DatabaseReference) in
            
            if let error = error {
                print("Data could not be saved: \(error)")
            }
            else {
                database.child("review_ids").child(userData.currentUser.id).child(lessonReview.id).setValue(lessonReview.isComplete) {
                    (error: Error?, ref:DatabaseReference) in
                    
                    if error != nil {
                        print("*** Review_ids was NOT updated ***")
                    }
                    else {
                        print("*** Review_ids was updated ***")
                        
                        self.presentationMode.wrappedValue.dismiss()
                        
                    }
                }
                print("Client review saved successfully")
            }
        }
    }
}
