//
//  ReviewList.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/20/21.
//

import SwiftUI

struct AdminReviewsList: View {
    let user: DbUser

    
    @EnvironmentObject var userData: UserData
    @State private var searchItem = ""

    @State private var showModal = false

    var body: some View {
        VStack {
            if userData.allReviews[user.id] == nil || userData.allReviews[user.id]!.isEmpty {
                VStack {

                    HStack {
                        Text("\(user.firstName) has no reviews")
                            .multilineTextAlignment(.center)
                            .frame(width: 200, alignment: .center)
                            .font(Font.subheadline.weight(.semibold))
                            .padding(10)
                            .opacity(0.6)
                    } // HStack
                } // VStack
            }
            else {
                ZStack {
                    backgroundGradient.ignoresSafeArea(.all)
                    VStack {
                        SearchBar(placeholder: "Search reviews by date or instructor", text: $searchItem)
                        List {
                            ForEach(Array(userData.allReviews[user.id]!.values).filter({ aReview in
                                return searchItem.isEmpty
                                    || aReview.submittedBy.localizedStandardContains(searchItem)
                                    || aReview.lessonDate.localizedStandardContains(searchItem)
                                
                            }).sorted(by: { (lessonA, lessonB) -> Bool in
                                return lessonA.lessonDate > lessonB.lessonDate
                            }), id: \.id) { aLessonReview in
                                NavigationLink(destination: ReviewDetails(user: self.user, lessonReview: aLessonReview)) {
                                    ReviewItem(user: self.user, lessonReview: aLessonReview)
                                }
                            }
                                .onDelete(perform: { indexSet in
                                    for i in indexSet {
            
                                        let reviewToDelete = Array(userData.allReviews[user.id]!.values).sorted(by: { (lessonA, lessonB) -> Bool in
                                            return lessonA.lessonDate > lessonB.lessonDate
                                        })[i]
                                        /**
                                         PLEASE NOTE THAT THIS FUNCTIONALITY IS STILL BEING TESTED.
                                         */
                                        print("Going to delete review: \(reviewToDelete.id)")
                                        deleteLessonReviewOfUserWithId(reviewId: reviewToDelete.id, userId: user.id)
            
                                    }
                                })
                            }
                            .listStyle(InsetGroupedListStyle())
                            .font(.system(size: 14))
                    }
                }
                
                
            }
                

        } // End of vstack
        .navigationBarTitle(Text("\(user.firstName)'s Reviews"))
        .navigationBarItems(trailing:
                                Button(action: {
                                    self.showModal = true
                                }) {
            Image(systemName: "plus")
                .foregroundColor(.accentColor)
        })
        .sheet(isPresented: $showModal) {
            LessonReviewAdminForm(user: self.user)
        }
    }
}
