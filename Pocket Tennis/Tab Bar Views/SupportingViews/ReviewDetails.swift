//
//  ReviewDetails.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/20/21.
//

import SwiftUI

struct ReviewDetails: View {
    let user: DbUser
    let lessonReview: LessonReview
    @EnvironmentObject var userData: UserData

    
    @State private var currView = "Instructor Review"
    let viewChoices = ["Instructor Review", "Client Review"]
    @State private var showModal = false
    @State private var columns: [GridItem] = [
        GridItem(.fixed(160), spacing: 0, alignment: .leading),
        GridItem(.fixed(80), spacing: 0, alignment: .center),
        GridItem(.fixed(70), spacing: 0, alignment: .center)
    ]
    
    var body: some View {
        ZStack {
            backgroundGradient.ignoresSafeArea(.all)
            
            VStack {
                
                Picker(selection: self.$currView, label: Text("Review To Display")) {
                    ForEach(viewChoices, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .frame(width: 350, height: 50, alignment: .center)
                
                // Show admin review or client review
                if currView == "Instructor Review" {
                    
                    if lessonReview.admin == nil {
                        if (!userData.currentUser.isAdmin) {
                            Spacer()

                            Text("\(self.lessonReview.submittedBy) has not completed their review")
                                .multilineTextAlignment(.center)
                                .frame(width: 200, alignment: .center)
                                .font(Font.subheadline.weight(.semibold))
                                .padding(10)
                                .foregroundColor(.accentColor)
                                .background(
                                    RoundedRectangle(cornerRadius: 16)
                                        .foregroundColor(.white)
                                        .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                )
                            Spacer()

                        }
                        else {
                            Spacer()
                            HStack {
                                Button(action: {
                                    self.showModal = true
                                }) {
                                    Text("Click to complete \nLesson Review")
                                        .multilineTextAlignment(.center)
                                        .frame(width: 200, alignment: .center)
                                        .font(Font.subheadline.weight(.semibold))
                                        .padding(10)
                                        .foregroundColor(.accentColor)
                                        .background(
                                            RoundedRectangle(cornerRadius: 16)
                                                .foregroundColor(.white)
                                                .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                        )
                                }
                            }
                            Spacer()
                            
                        }
                    }
                    else {
                        let adminReview = lessonReview.admin! as AdminReview
                        
                        Form {
                            Section(header: Text("Submitted By")) {
                                Text(lessonReview.submittedBy)
                            }
                            
                            Section(header: Text("Notes")) {
                                Text(adminReview.notes)
                                    .multilineTextAlignment(.leading)
                            }
                            Section(header: Text("Strokes")) {
                                LazyVGrid(columns: columns, spacing: 25, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/) {
                                    Section() {
                                        Text("")
                                        Text("Current")
                                        Text("Next")
                                    }
                                    .padding(.top, 10)
                                    Section() {
                                        Text("Forehand")
                                        Toggle("", isOn: .constant(adminReview.forehandCurr))
                                        Toggle("", isOn: .constant(adminReview.forehandNext))
                                        Text("Backhand")
                                        Toggle("", isOn: .constant(adminReview.backhandCurr))
                                        Toggle("", isOn: .constant(adminReview.backhandNext))
                                        Text("Forehand Volley")
                                        Toggle("", isOn: .constant(adminReview.forehandVolleyCurr))
                                        Toggle("", isOn: .constant(adminReview.forehandVolleyNext))
                                    }
                                    Section() {
                                        Text("Backhand Volley")
                                        Toggle("", isOn: .constant(adminReview.backhandVolleyCurr))
                                        Toggle("", isOn: .constant(adminReview.backhandVolleyNext))
                                        Text("Reverse Forehand")
                                        Toggle("", isOn: .constant(adminReview.reverseForehandCurr))
                                        Toggle("", isOn: .constant(adminReview.reverseForehandNext))
                                        Text("Dropshot")
                                        Toggle("", isOn: .constant(adminReview.dropshotCurr))
                                        Toggle("", isOn: .constant(adminReview.dropshotNext))
                                    }
                                    Section() {
                                        Text("Lob")
                                        Toggle("", isOn: .constant(adminReview.lobCurr))
                                        Toggle("", isOn: .constant(adminReview.lobNext))
                                        Text("Serve")
                                        Toggle("", isOn: .constant(adminReview.serveCurr))
                                        Toggle("", isOn: .constant(adminReview.serveNext))
                                        Text("Return")
                                        Toggle("", isOn: .constant(adminReview.returnServeCurr))
                                        Toggle("", isOn: .constant(adminReview.returnServeNext))
                                    }
                                }
                                .font(.system(size: 20))
                                .padding(.bottom, 10)
                            }
                            Section(header: Text("Skills")) {
                                LazyVGrid(columns: columns, spacing: 25, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/) {
                                    Section() {
                                        Text("")
                                        Text("Current")
                                        Text("Next")
                                    }
                                    .padding(.top, 10)
                                    Section() {
                                        Text("Quickness")
                                        Toggle("", isOn: .constant(adminReview.quicknessCurr))
                                        Toggle("", isOn: .constant(adminReview.quicknessNext))
                                        Text("Agility")
                                        Toggle("", isOn: .constant(adminReview.agilityCurr))
                                        Toggle("", isOn: .constant(adminReview.agilityNext))
                                        Text("Balance")
                                        Toggle("", isOn: .constant(adminReview.balanceCurr))
                                        Toggle("", isOn: .constant(adminReview.balanceNext))
                                    }
                                    Section() {
                                        Text("Reaction Time")
                                        Toggle("", isOn: .constant(adminReview.reactionTimeCurr))
                                        Toggle("", isOn: .constant(adminReview.reactionTimeNext))
                                        Text("Aerobic")
                                        Toggle("", isOn: .constant(adminReview.aerobicCurr))
                                        Toggle("", isOn: .constant(adminReview.aerobicNext))
                                        Text("Strategy")
                                        Toggle("", isOn: .constant(adminReview.strategyCurr))
                                        Toggle("", isOn: .constant(adminReview.strategyNext))
                                    }
                                    Section() {
                                        Text("Mental Toughness")
                                        Toggle("", isOn: .constant(adminReview.mentalToughnessCurr))
                                        Toggle("", isOn: .constant(adminReview.mentalToughnessNext))
                                    }
                                }
                                .font(.system(size: 20))
                                .padding(.bottom, 10)
                            }
                        } // End of Form
                        .font(.system(size: 20))
                        .toggleStyle(CheckboxToggleStyle())

                    }
                }
                else {
                    if lessonReview.client == nil {
                        if (userData.currentUser.isAdmin) {
                            Spacer()
                            Text("\(self.user.firstName) has not completed their review")
                                .multilineTextAlignment(.center)
                                .frame(width: 200, alignment: .center)
                                .font(Font.subheadline.weight(.semibold))
                                .padding(10)
                                .foregroundColor(.accentColor)
                                .background(
                                    RoundedRectangle(cornerRadius: 16)
                                        .foregroundColor(.white)
                                        .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                )
                            Spacer()
                        }
                        else {
                            Spacer()
                            HStack {
                                Button(action: {
                                    self.showModal = true
                                }) {
                                    Text("Click to complete \nLesson Review")
                                        .multilineTextAlignment(.center)
                                        .frame(width: 200, alignment: .center)
                                        .font(Font.subheadline.weight(.semibold))
                                        .padding(10)
                                        .foregroundColor(.accentColor)
                                        .background(
                                            RoundedRectangle(cornerRadius: 16)
                                                .foregroundColor(.white)
                                                .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                        )
                                }
                            }
                            Spacer()
                            
                        }
                    }
                    else {
                        let clientReview = lessonReview.client! as ClientReview
                        Form {
                            Section(header: Text("Notes")) {
                                Text(clientReview.notes)
                                    .multilineTextAlignment(.leading)
                            }
                            Section(header: Text("On a scale from 1-5...")) {
                                HStack (alignment: .top, spacing: 38) {
                                    VStack {
                                        Text("1")
                                        Text("Bad")
                                    }
                                    VStack {
                                        Text("2")
                                        Text("").frame(height: 24)
                                    }
                                    VStack {
                                        Text("3")
                                        Text("Okay")
                                    }
                                    VStack {
                                        Text("4")
                                        Text("").frame(height: 24)
                                    }
                                    VStack {
                                        Text("5")
                                        Text("Great")
                                    }
                                } //End of HStack
                            } // End of Section
                            Section {
                                Text("How did your ___ feel?")
                                VStack {
                                    Text("Head")
                                    Slider(value: .constant(clientReview.head), in: 1...5, step: 1)
                                    Text("\(Int(clientReview.head))")
                                }
                                VStack {
                                    Text("Heart")
                                    Slider(value: .constant(clientReview.heart), in: 1...5, step: 1)
                                    Text("\(Int(clientReview.heart))")
                                }
                                VStack {
                                    Text("Lungs")
                                    Slider(value: .constant(clientReview.lungs), in: 1...5, step: 1)
                                    Text("\(Int(clientReview.lungs))")
                                }
                                VStack {
                                    Text("Legs")
                                    Slider(value: .constant(clientReview.legs), in: 1...5, step: 1)
                                    Text("\(Int(clientReview.legs))")
                                }
                            } //End of Section
                            Section(header: Text("How much fun did you have?")) {
                                HStack (alignment: .top, spacing: 34) {
                                    VStack {
                                        Text("1")
                                        Text("None")
                                    }
                                    VStack {
                                        Text("2")
                                        Text("").frame(height: 24)
                                    }
                                    VStack {
                                        Text("3")
                                        Text("Some")
                                    }
                                    VStack {
                                        Text("4")
                                        Text("").frame(height: 24)
                                    }
                                    VStack {
                                        Text("5")
                                        Text("A ton")
                                    }
                                } //End of HStack
                                VStack {
                                    Text("Fun Meter")
                                    Slider(value: .constant(clientReview.funMeter), in: 1...5, step: 1)
                                    Text("\(Int(clientReview.funMeter))")
                                }
                            } // End of section
                        } // End of Form
                        .font(.system(size: 20))
                        .padding(.bottom, 10)
                    }
                }
                Spacer()
            }// End of VStack
            .navigationBarTitle(Text(lessonReview.lessonDate))
            .sheet(isPresented: $showModal) {
                
                if self.currView == "Coach Review" {
                    LessonReviewAdminForm(user: self.user).environmentObject(self.userData)
                }
                else {
                    LessonReviewClientForm(lessonReview: self.lessonReview).environmentObject(self.userData)
                }
            }
        }
        
    }
    
}

