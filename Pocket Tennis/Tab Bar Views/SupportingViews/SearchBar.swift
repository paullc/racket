//
//  SearchBar.swift
//  Skilla
//
//  Created by Pau Lleonart Calvo on 12/6/20.
//  Copyright © 2020 SkillaTeam. All rights reserved.
//

import SwiftUI
 
struct SearchBar: View {
    var placeholder: String
    
    @Binding var text: String
    @Environment(\.colorScheme) var colorScheme

  
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .foregroundColor(.secondary)
            TextField(placeholder, text: $text)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .font(.system(size: 14))
            if text != "" {
                Text("Clear")
                    .foregroundColor(.accentColor)
                    .padding(3)
                    .onTapGesture {
                        withAnimation {
                            self.text = ""
                          }
                    }
                    .font(.system(size: 14))

            }
        }
        .padding(10)
        .background(self.backgroundColor)
        .cornerRadius(12)
        .padding(.vertical, 10)
        .padding(.horizontal, 10)

    }
    

    var backgroundColor: Color {
      if colorScheme == .dark {
           return Color(.systemGray5)
       } else {
           return Color(.systemGray6)
       }
    }
}
