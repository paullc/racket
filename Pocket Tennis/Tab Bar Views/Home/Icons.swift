//
//  Icons.swift
//  Pocket Tennis
//
//  Created by Harlan  Reese Pounders on 4/23/21.
//

import SwiftUI

struct Icon: View {
    var imageName: String
    var title: String
    
    var body: some View {
        ZStack {
            Image(imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 140, height: 140, alignment: .center)
                .cornerRadius(30)
                .shadow(radius: 10)
            Text(title)
                .font(.system(size: 20))
                .bold()
                .foregroundColor(.white)
                .opacity(0.8)
                .shadow(color: .black, radius: 1)
                .frame(width: 110, height: 120, alignment: .bottomLeading)
        }
    }
}

struct WideIcon: View {
    var imageName: String
    var title: String
    
    var body: some View {
        ZStack {
            Image(imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 270, height: 120, alignment: .center)
                .cornerRadius(30)
                .shadow(radius: 10)
            Text(title)
                .font(.system(size: 25))
                .bold()
                .foregroundColor(.white)
                .opacity(0.8)
                .shadow(color: .black, radius: 1)
                .frame(width: 230, height: 100, alignment: .bottomLeading)
        }
    }
}
