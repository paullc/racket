//
//  AboutAnne.swift
//  Racket
//
//  Created by Harlan  Reese Pounders on 4/14/21.
//

import SwiftUI

struct AboutAnne: View {
    var body: some View {
        NavigationView {
        ZStack {
            backgroundGradient
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack {
                    HStack {
                        Spacer()
                        VStack {
                            Text("ANNE")
                            Text("JONES")
                            Text("THOMPSON")
                        }
                        .font(.system(size: 30))
                        .shadow(radius: 20)
                        Spacer()
                        Image("AnneJones1")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .cornerRadius(20)
                            .shadow(radius: 7)
                            .frame(height: 155)
                        Spacer()
                    }
                    
                    Spacer()
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 10)
                            .foregroundColor(Color.gray.opacity(0.7))
                            .shadow(radius: 5)
                            .frame(width: 250)
                        VStack(alignment: .leading) {
                            Text("Email: triten@comcast.net")
                            Text("Phone: (540) 230-3019")
                        }
                        .shadow(color: .black, radius: 2)
                        .padding()
                    }
                    
                    Spacer()
                    
                    HStack {
                        Spacer(minLength: 10)
                        Image("AnneJonesYoung")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .cornerRadius(20)
                            .shadow(radius: 7)
                            .frame(height: 155)
                        Spacer(minLength: 10)
                        Text("Anne Jones Thompson and her sister were introduced to tennis at age nine. In high school, they knew they wanted to play at the collegaite level.")
                            .multilineTextAlignment(.trailing)
                            .frame(height: 200)
                        Spacer(minLength: 10)
                    }
                    
                    Spacer()
                    
                    Text("She and her sister eventually became the first women to receive athletic scholarships at Virginia Tech.")
                        .multilineTextAlignment(.center)
                        .padding()
                        .frame(height: 100)
                    
                    Spacer()
                    
                    HStack {
                        Spacer()
                        Text("Tennis is not her only hobby, Anne has done 40 years of triathlons, marathons, adventure races, iron man challenges, biking, hiking, and everything inbetween.")
                            .frame(height: 150)
                        Spacer()
                        Image("AnneJonesBiking")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .cornerRadius(20)
                            .shadow(radius: 7)
                            .frame(height: 180)
                        Spacer()
                    }
                } // End of VStack
                VStack {
                    Spacer()
                    
                    HStack {
                        Spacer()
                        Image("AnneJonesFire")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .cornerRadius(20)
                            .shadow(radius: 7)
                            .frame(width: 180)
                        Spacer()
                        Text("She has even completed a 40 hour race consisting of trail running, mountain biking, water sports, and rope climbing")
                            .multilineTextAlignment(.trailing)
                        Spacer()
                    }
                    
                    Spacer()
                    
                    Text("When she is not running marathons, playing doubles tournaments, biking the Blue Ridge Parkway, or hiking the Grand Canyon, you can find Anne teaching tennis lessons at the Burrows-Burleson Tennis Center here on campus.")
                        .multilineTextAlignment(.center)
                        .padding()
                    Image("AnneJonesTeaching")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(20)
                        .shadow(radius: 7)
                        .padding()
                } // End of VStack
            } // End of ScrollView
            .font(.system(size: 18))
            .shadow(radius: 30)
        } // End of ZStack
        .navigationBarHidden(true)
        } // End of NavigationView
    } // End of body
}

struct AboutAnne_Previews: PreviewProvider {
    static var previews: some View {
        AboutAnne()
    }
}
