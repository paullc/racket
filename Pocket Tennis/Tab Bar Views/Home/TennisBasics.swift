//
//  TennisBasics.swift
//  Racket
//
//  Created by Harlan  Reese Pounders on 4/15/21.
//

import SwiftUI

struct TennisBasics: View {
    @EnvironmentObject var userData: UserData
    // let tutorial:TutorialStruct
    var body: some View {
            ZStack {
                backgroundGradient
                    .edgesIgnoringSafeArea(.all)
                ScrollView {
                    VStack {
                        Spacer(minLength: 40)
                        HStack {
                            Spacer(minLength: 30)
                            NavigationLink(destination: Icon1()) {
                                Icon(imageName: "TennisRacket", title: "Racket")
                            }
                            Spacer(minLength: 30)
                            NavigationLink(destination: Icon2()) {
                                Icon(imageName: "TennisFrame", title: "Frame")
                            }
                            Spacer(minLength: 30)
                        }
                        Spacer(minLength: 30)
                        HStack {
                            Spacer(minLength: 30)
                            NavigationLink(destination: Icon3()) {
                                Icon(imageName: "TennisGrip", title: "Grip")
                            }
                            Spacer(minLength: 30)
                            NavigationLink(destination: Icon4()) {
                                Icon(imageName: "TennisCost", title: "Cost")
                            }
                            Spacer(minLength: 30)
                        }
                        Spacer(minLength: 30)
                        Text("HAVE FUN!!")
                            .font(.system(size: 30))
                            .foregroundColor(.white)
                            .shadow(color: .black, radius: 5)
                            .padding()
                    } // End of VStack
                } // End of ScrollView
                .font(.system(size: 18))
                .shadow(radius: 30)
            } // End of ZStack
    }
}
struct Icon1:View {
    @EnvironmentObject private var userData: UserData
    var body :some View {
        ZStack {
            backgroundGradient
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack (alignment: .center){
                    Spacer()
                    Text("Choosing a Tennis Racket")
                        .font(.system(size: 25))
                        .bold()
                    Spacer()
                    Image("TennisBasics1")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(30)
                        .shadow(radius: 10)
                        .frame(height: 300)
                    Spacer()
                }
                VStack {
                    Text("When choosing a tennis racket, there are a few questions to consider:")
                        .bold()
                        .multilineTextAlignment(.center)
                    Spacer()
                    ZStack {
                        RoundedRectangle(cornerRadius: 10)
                            .foregroundColor(Color.gray.opacity(0.7))
                        VStack(alignment: .leading) {
                            Text("-Will you need help creating power for your shots? Or will you need more control?")
                            Spacer()
                            Text("-Do you expect to remain a recreational player, or do you want to become more competitive?")
                            Spacer()
                            Text("-What is your budget?")
                        }
                        .padding()
                        .shadow(color: .black, radius: 0)
                    }
                    Spacer()
                    Text("How you answer these questions will help you find the perfect entry level racket.")
                        .bold()
                        .multilineTextAlignment(.center)

                    Spacer()
                }
            } // End of ScrollView
            .font(.system(size: 18))
            .padding()
        } // End of ZStack
        .navigationBarTitle("Racket")
    }
}



struct Icon2:View {
    @EnvironmentObject private var userData: UserData
    var body :some View {
        ZStack {
            backgroundGradient
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack (alignment: .center){
                    Text("Choosing your Frame Size")
                        .font(.system(size: 25))
                        .bold()
                    Image("TennisBasics2")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(30)
                        .shadow(radius: 10)
                        .frame(height: 180)
                    ZStack {
                        RoundedRectangle(cornerRadius: 10)
                            .foregroundColor(Color.gray.opacity(0.7))
                        VStack(alignment: .leading) {
                            Text("-Beginners will want to pick a racket with a pretty large head. Smaller heads generate more power. Larger head sizes have a larger sweet spot and offer more control.")
                            Spacer()
                            Text("-Lighter frames are easier to swing, but have less momentum on contact with the ball. Beginners rackets should be lighter in weight so that it is easier to swing.")
                            Spacer()
                            Text("-The strings on a tennis racket should always be strung tight to make good contact. Beginners should make sure their racket is already strung or strung by a professional.")
                        }
                        .padding()
                        .multilineTextAlignment(.leading)
                        .frame(height: 450)
                    }
                    .padding()
                    Image("TennisBasics3")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(30)
                        .shadow(radius: 10)
                        .frame(height: 180)
                } // End of VStack
                .frame(width: 330)
            } // End of ScrollView
            .font(.system(size: 18))
            .shadow(color: .black, radius: 2)
            .padding()
        } // End of ZStack
        .navigationBarTitle("Frame")
    }
}
struct Icon3:View {
    @EnvironmentObject private var userData: UserData
    var body :some View {
        ZStack {
            backgroundGradient
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack (alignment: .center){
                    Text("Choosing your Grip Size")
                        .font(.system(size: 25))
                        .bold()
                    Spacer(minLength: 20)
                    Text("Grip size has a major influence on your performance. A proper grip will improve your control over the tennis racket.")
                        .bold()
                    Spacer(minLength: 10)
                    Text("Use the tests below to find your grip size.")
                        .bold()
                    Image("TennisBasics4")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(30)
                        .shadow(radius: 10)
                        .frame(height: 250)
                    ZStack {
                        RoundedRectangle(cornerRadius: 10)
                            .foregroundColor(Color.gray.opacity(0.7))
                        Text("When holding the racket as seen below, your index finger should fit snugly between your fingers and palm of your hand, with little or no space between. This will give you the most comfortable and secure grip.")
                            .padding()
                            .multilineTextAlignment(.leading)
                            .frame(height: 200)
                    }
                    .padding()
                    Image("TennisBasics5")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(30)
                        .shadow(radius: 10)
                        .frame(height: 180)
                } // End of VStack
                .frame(width: 330)
            } // End of ScrollView
            .font(.system(size: 18))
            .padding()
        } // End of ZStack
        .navigationBarTitle("Grip")
    }
}
struct Icon4:View {
    @EnvironmentObject private var userData: UserData
    var body :some View {
        ZStack {
            backgroundGradient
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack (alignment: .center){
                    Text("Buying a Racket")
                        .font(.system(size: 25))
                        .bold()
                    Spacer(minLength: 20)
                    Text("Aluminum rackets are a good choice for beginners. They are usually cheap and sold pre-strung")
                        .bold()
                    ZStack {
                        RoundedRectangle(cornerRadius: 25)
                            .foregroundColor(Color.gray.opacity(0.7))
                        VStack {
                            Spacer()
                            Text("High end rackets are made with graphite composites that incorporate materials such as titanium, kevlar or fibreglass. These rackets are lightweight and rigid, making them more powerful, accurate, and long lasting. These rackets can cost $60-$200+")
                                .padding()
                                .multilineTextAlignment(.leading)
                                //.frame(height: 200)
                            Image("TennisBasics6")
                                .renderingMode(.original)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .cornerRadius(20)
                                .shadow(radius: 10)
                                .frame(height: 180)
                            Text("Cross section of Frame")
                                .foregroundColor(.accentColor)
                            Spacer()
                        }
                        
                    }
                    Text("Demos at local tennis shops and borrowing rackets from friends can help you determine the correct racket for you.")
                        .bold()
                        .padding()
                } // End of VStack
                .frame(width: 330)
            } // End of ScrollView
            .font(.system(size: 18))
            .padding()
        } // End of ZStack
        .navigationBarTitle("Cost")
    }
}

