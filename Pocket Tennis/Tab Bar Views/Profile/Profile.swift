//
//  Profile.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/20/21.
//

import SwiftUI
import FirebaseAuth

struct Profile: View {
    
    @EnvironmentObject var userData: UserData
    @Environment(\.colorScheme) var colorScheme
    
    @State private var photoTakeOrPickIndex = 1     // Pick from Photo Library
    var photoTakeOrPickChoices = ["Camera", "Photo Library"]
    @State private var showImagePicker = false
    @State private var photoImageData: Data? = nil
    
    @State private var showDeleteAccountAlert = false

    var body: some View {
        NavigationView {
            ZStack {
                /*
                 Create linear gradient background
                 */
                backgroundGradient
                    .edgesIgnoringSafeArea(.all)
                
                VStack(alignment: .center, spacing: 20) {

                    Spacer().frame(width: 10, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Form {
                        Group {
                            Section(header: Text("First Name")) {
                                Text(userData.currentUser.firstName)
                                    .multilineTextAlignment(.leading)
                            }
                            Section(header: Text("Last Name")) {
                                Text(userData.currentUser.lastName)
                                    .multilineTextAlignment(.leading)
                            }
                            Section(header: Text("USTA ID")) {
                                NavigationLink(destination: ChangeSetting(attribute: "USTA ID")) {
                                    Text(userData.currentUser.ustaNumber)
                                        .multilineTextAlignment(.leading)
                                }
                            }
                        }
                        
                        Section(header: Text("Universal Tennis Rating")) {
                            NavigationLink(destination: ChangeSetting(attribute: "UTR")) {
                                Text(userData.currentUser.utrNumber)
                                    .multilineTextAlignment(.leading)
                            }
                        }
                        
                        Section(header: Text("Date of Birth")) {
                            NavigationLink(destination: ChangeSetting(attribute: "Birthday")) {
                                Text(userData.currentUser.birthday)
                                    .multilineTextAlignment(.leading)
                            }
                        }
                        Section(header: Text("Phone Number")) {
                            NavigationLink(destination: ChangeSetting(attribute: "Phone Number")) {
                                Text(formatPhone(phoneNumber: userData.currentUser.phone) ?? userData.currentUser.phone)
                                    .multilineTextAlignment(.leading)
                            }
                        }
                        
                        Section(header: Text("Email")) {
                            NavigationLink(destination: ChangeSetting(attribute: "Email")) {
                                Text(userData.currentUser.email)
                                    .multilineTextAlignment(.leading)
                            }
                        }
                        
                        Section(header: Text("Password")) {
                            NavigationLink(destination: ChangeSetting(attribute: "Password")) {
                                Text("Change Password")
                                    .multilineTextAlignment(.leading)
                            }
                        }
                        
                        
                        Section(header: Text("Change Profile Photo")) {
                            VStack {
                                Picker("Take or Pick Photo", selection: $photoTakeOrPickIndex) {
                                    ForEach(0 ..< photoTakeOrPickChoices.count, id: \.self) {
                                        Text(self.photoTakeOrPickChoices[$0])
                                    }
                                }
                                .pickerStyle(SegmentedPickerStyle())
                                .padding()
                                Divider()
                                Button(action: {
                                    self.showImagePicker = true
                                }) {
                                    Text("Get Profile Photo")
                                        .padding()
                                }
                                
                            }   // End of VStack

                        } // End of section
                        
                        if(userData.currentUser.profilePic == Image(systemName: "person.circle")) {
                            VStack {
                                HStack {
                                    Spacer()
                                    userData.currentUser.profilePic
                                        .resizable()
                                        .imageScale(.medium)
//                                        .aspectRatio(contentMode: .fit)
                                        .foregroundColor(Color.accentColor)
                                        .shadow(radius: 15)
                                        .frame(width: 100, height: 100)
                                    Spacer()
                                }
                                
                            }
                            .padding(.top, 15)
                            .padding(.bottom, 15)

                            
                        }
                        else {
                            userData.currentUser.profilePic
                                .resizable()
                                .imageScale(.medium)
                                .aspectRatio(contentMode: .fit)
                                .cornerRadius(10)
                                .overlay(RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color.accentColor, lineWidth: 4))
                        }
                                            
                        
                        Section(header: Text("")) {
                            Button(action: {
                                logOutCurrentUser(userData: userData)
                            }, label: {
                                HStack {
                                    Spacer()
                                    Text("Log Out")
                                    Spacer()
                                }
                            })
                        }
                        
                        if !userData.currentUser.isAdmin {
                            Section(header: Text("")) {
                                Button(action: {
                                    self.showDeleteAccountAlert = true
                                }) {
                                    HStack {
                                        Spacer()
                                        Text("Delete Account")
                                        Spacer()
                                    }
                                }
                            }
                        }
                        
                    
                    }// end of Form
                    .font(.system(size: 14))
                    .alert(isPresented: $showDeleteAccountAlert, content: {
                        self.deleteAccountAlert
                    })

                }// end of vstack
                .sheet(isPresented: self.$showImagePicker) {
                    /*
                     🔴 We pass $showImagePicker and $photoImageData with $ sign into PhotoCaptureView
                     so that PhotoCaptureView can change them. The @Binding keywork in PhotoCaptureView
                     indicates that the input parameter is passed by reference and is changeable (mutable).
                     */
                    PhotoCaptureView(showImagePicker: self.$showImagePicker,
                                     photoImageData: Binding(
                                        get: { self.photoImageData },
                                        set: { (newImageData) in
                                            self.photoImageData = newImageData
                                            if newImageData != nil {
                                                uploadImageToCloud(imageData: newImageData!, userID: userData.currentUser.id)
                                                userData.currentUser.profilePic = getImageFromBinaryData(binaryData: newImageData, defaultSystemName: "person.circle")
                                            }
                                        }
                                     ),
                                     cameraOrLibrary: self.photoTakeOrPickChoices[self.photoTakeOrPickIndex])
                } //End of VStack
                .navigationBarTitle(Text("My Profile"))
                
            }// end of zstack
        }
        
        
    }// end of body
    
    var deleteAccountAlert: Alert {
        Alert(title: Text("Delete Account?"),
              message: Text("This account will be permanently deleted and its data and reviews cannot be recovered"),
              primaryButton: .default(Text("OK")) {
                deleteCurrentUserAccount(userData: userData)
              },
              secondaryButton: .default(Text("Cancel")) {
                print("Cancelled operation")
              })

    }
    
    /*
     Presents the Image Data
     */
    var photoImage: Image {
        
        if let imageData = self.photoImageData {
            // The public function is given in UtilityFunctions.swift
            let imageView = getImageFromBinaryData(binaryData: imageData, defaultSystemName: "person.circle")
            return imageView
        } else {
            return Image("TriTennisLogo")
        }
    }
    
    func uploadImageToCloud(imageData: Data, userID: String) {
        
        storage.child("images/\(userID).jpg").putData(imageData, metadata: nil, completion: { _, error in
            guard error == nil else {
                print("IMAGE UPLOAD FAILED")
                return
            }
            
            print("IMAGE UPLOAD SUCCEEDED")
            
        })
    }
}
